package models;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.libs.F;
import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.*;

@Entity
public class Student extends Model implements PathBindable<Student>,
        QueryStringBindable<Student> {
    @Id
    public Long id;

    @Constraints.Required
    public String mssv;

    @Constraints.Required
    public String name;

    public String birthDay;
    public String classStudent;
    public String faculty;

    public static Finder<Long, Student> find = new Finder<Long, Student>(Long.class, Student.class);

    public Student(){}

    public Student(String mssv, String name, String birthDay){
        this.mssv = mssv;
        this.name = name;
        this.birthDay = birthDay;
    }
    public Student(String mssv, String name, String birthDay, String classStudent, String faculty){
        this.mssv = mssv;
        this.name = name;
        this.birthDay = birthDay;
        this.faculty = faculty;
        this.classStudent = classStudent;
    }

    public String toString() {
        return String.format("%s (%s)", name, mssv);
    }

    public static Student findByMssv(String mssv) {
        return find.where().eq("mssv", mssv).findUnique();
    }
    public static Student findById(Long id) {
        return find.where().eq("id", id).findUnique();
    }

    public static Page<Student> find(int page) {
        return
                find.where()
                        .orderBy("mssv asc")
                        .findPagingList(5)
                        .setFetchAhead(false)
                        .getPage(page);
    }

    public void delete() {
        super.delete();
    }

    @Override
    public Student bind(String key, String value) {
        return findByMssv(value);
    }

    @Override
    public F.Option<Student> bind(String key, Map<String, String[]> data) {
        return F.Option.Some(findByMssv(data.get("mssv")[0]));
    }

    @Override
    public String unbind(String s) {
        return this.mssv;
    }

    @Override
    public String javascriptUnbind() {
        return this.mssv;
    }
}
