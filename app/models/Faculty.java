
package models;

import com.avaje.ebean.Expression;
import com.avaje.ebean.ExpressionList;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.*;


@Entity
public class Faculty extends Model {
    @Id
    public Long id;
    @Constraints.Required
    public String name;

    //public List<Student> students;

    public Faculty() {
    }

    public Faculty(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Map<String, Boolean> makeFacultyMap(Student student) {
        Map<String, Boolean> facultyMap = new TreeMap<String, Boolean>();
        for (Faculty faculty : Faculty.findAll()) {
            facultyMap.put(faculty.getName(), (student == null) ? false : (student.faculty != null && student.faculty.equals(faculty.getName())));
        }
        Map<String, Boolean> sortedMapAsc = sortByComparator(facultyMap, false);
        return sortedMapAsc;
    }

    public static Faculty findById(Long id) {
        return Faculty.find.byId(id);
    }

    public static Model.Finder<Long, Faculty> find = new Model.Finder<Long,Faculty>(Long.class, Faculty.class);

    public static List<Faculty> findAll() {
        List<Faculty> faculties = Faculty.find.all();
        return faculties;

    }

    public static Map<String, Boolean> sortByComparator(Map<String, Boolean> unsortMap, final boolean order) {

        List<Map.Entry<String, Boolean>> list = new LinkedList<>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Boolean>>() {
            @Override
            public int compare(Map.Entry<String, Boolean> o1, Map.Entry<String, Boolean> o2) {
                if (order) {
                    return o1.getValue().compareTo(o2.getValue());
                } else {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });
        Map<String, Boolean> sortedMap = new LinkedHashMap<>();
        for (Map.Entry<String, Boolean> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}