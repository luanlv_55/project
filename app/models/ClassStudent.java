
package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.*;

@Entity
public class ClassStudent {
    @Id
    public Long id;
    @Constraints.Required
    public String name;
    public ClassStudent(){}

    public ClassStudent(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Map<String, Boolean> makeClassStudentMap(Student student) {
        Map<String, Boolean> classStudentMap = new TreeMap<String, Boolean>();
        for (ClassStudent classStudent : ClassStudent.findAll()) {
            classStudentMap.put(classStudent.getName(),  (student == null) ? false : (student.classStudent != null && student.classStudent.equals(classStudent.getName())));
        }
        Map<String, Boolean> sortedMapAsc = Faculty.sortByComparator(classStudentMap, false);
        return sortedMapAsc;
    }

    public static Model.Finder<Long,ClassStudent> find = new Model.Finder<Long,ClassStudent>(Long.class, ClassStudent.class);

    public static List<ClassStudent> findAll() {
        List<ClassStudent> allClass = ClassStudent.find.all();
        return allClass;
    }
}
