import play.*;
import play.api.mvc.EssentialFilter;
import play.filters.csrf.CSRFFilter;
import play.libs.*;
import java.util.*;
import play.data.format.Formatters;
import play.data.format.Formatters.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.avaje.ebean.*;

import models.*;


public class Global extends GlobalSettings {

    public void onStart(Application app) {
        InitialData.insert(app);
    }

    static class InitialData {

        public static void insert(Application app) {
            if(Ebean.find(Student.class).findRowCount() == 0) {

                Map<String,List<Object>> all = (Map<String,List<Object>>)Yaml.load("initial-data.yml");
/*                if (Ebean.find(Student.class).findRowCount() == 0) {
                    Ebean.save(all.get("students"));
                }*/
                if (Ebean.find(Faculty.class).findRowCount() == 0) {
                    Ebean.save(all.get("tag1"));
                }

                if (Ebean.find(ClassStudent.class).findRowCount() == 0) {
                    Ebean.save(all.get("tag2"));
                }
                if (Ebean.find(UserAccount.class).findRowCount() == 0) {
                    Ebean.save(all.get("user_accounts"));
                }
            }
        }

    }

    @Override
    public <T extends EssentialFilter> Class<T>[] filters() {
        return new Class[]{CSRFFilter.class};
    }
}