package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import models.ClassStudent;
import models.Faculty;
import models.Student;
import play.data.Form;
import play.mvc.*;
import views.html.students.details;
import views.html.students.list;


@Security.Authenticated(Secured.class)
public class Quanly extends Controller {
    public static Result index() {
        return redirect(routes.Quanly.list(0));
    }

    public static Result list(Integer page) {
        Page<Student> students = Student.find(page);
        return ok(views.html.catalog.render(students));
    }

    private static final Form<Student> studentForm = Form
            .form(Student.class);

    public static Result newStudent() {
        Student student = new Student();
        return ok(details.render(studentForm, Faculty.makeFacultyMap(student), ClassStudent.makeClassStudentMap(student)));
    }

    public static Result details(Student student) {
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(filledForm,
                Faculty.makeFacultyMap(filledForm.get()),
                ClassStudent.makeClassStudentMap(filledForm.get())));
    }

    public static Result save() {
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm,
                    Faculty.makeFacultyMap(boundForm.get()),
                    ClassStudent.makeClassStudentMap(boundForm.get())));
        }
        Student student = boundForm.get();
        if (student.id == null) {
            if(Student.findByMssv(student.mssv) == null) {
                flash("success", String.format("Successfully added student %s",  student));
                Ebean.save(student);
            }
        } else {
            flash("success", String.format("Successfully update student %s", student));
            Ebean.update(student);
        }
        return redirect(routes.Quanly.list(0));
    }

    public static Result delete(String mssv) {
        final Student student = Student.findByMssv(mssv);
        if(student == null) {
            return notFound(String.format("Student %s does not exists.", mssv));
        }
        student.delete();
        return redirect(routes.Quanly.list(0));
    }
}