# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table class_student (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_class_student primary key (id))
;

create table faculty (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_faculty primary key (id))
;

create table student (
  id                        bigint not null,
  mssv                      varchar(255),
  name                      varchar(255),
  birth_day                 varchar(255),
  class_student             varchar(255),
  faculty                   varchar(255),
  constraint pk_student primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user_account primary key (id))
;

create sequence class_student_seq;

create sequence faculty_seq;

create sequence student_seq;

create sequence user_account_seq;




# --- !Downs

drop table if exists class_student cascade;

drop table if exists faculty cascade;

drop table if exists student cascade;

drop table if exists user_account cascade;

drop sequence if exists class_student_seq;

drop sequence if exists faculty_seq;

drop sequence if exists student_seq;

drop sequence if exists user_account_seq;

